﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Apache.NMS;
using Apache.NMS.Util;

namespace FixCurrentApi.Controllers.ExternalAdapter.Prd
{
    public class PrdApiController : ApiController
    {
        [HttpPost]
        public async Task<string> GetNotification()
        {
            string hasilXml = "";
            string result = "";

            Task<string> req = Request.Content.ReadAsStringAsync();
            string jsonReq = req.Result;

            if (jsonReq.Trim().Contains("ARS-TESTINTEGRATION").Equals(true))
            {
                XmlDocument xmlDoc = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq, "root");
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes("root/Inserted");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string reqNbr = "";
                    string lineNbr = "";
                    string poNo = "";
                    string supplierNo = "";
                    string orderQty = "";
                    string unitCost = "";
                    string itemDesc = "";
                    string itemType = "";
                    string accountCode = "";
                    string projectNo = "";
                    string amount = "";
                    string customerNo = "";
                    string currency = "";
                    string vendorRef = "";
                    string type = "";
                    string promisedOn = "";
                    string termsID = "";
                    string leadTime = "";
                    string poDesc = "";
                    string orderDate = "";
                    string uom = "";
                    string freight = "";
                    string purchOfficer = "";
                    string position = "";
                    string dueDateOri = "";
                    string dueSiteOri = "";

                    reqNbr = xmlNode["ReqNbr"].InnerText;
                    lineNbr = xmlNode["LineNbr"].InnerText;
                    poNo = xmlNode["OrderNbr"].InnerText;
                    supplierNo = xmlNode["Vendor"].InnerText;
                    orderQty = xmlNode["OrderQty"].InnerText;
                    unitCost = xmlNode["UnitCost"].InnerText;
                    itemDesc = xmlNode["Description_2"].InnerText;
                    itemType = xmlNode["POType"].InnerText;
                    accountCode = xmlNode["AccountCode"].InnerText;
                    projectNo = xmlNode["DONumber"].InnerText;
                    amount = xmlNode["Amount"].InnerText;
                    customerNo = xmlNode["Customer"].InnerText;
                    currency = xmlNode["Currency"].InnerText;
                    vendorRef = xmlNode["VendorRef"].InnerText;
                    type = xmlNode["Type"].InnerText;
                    promisedOn = xmlNode["DueSite"].InnerText;
                    termsID = xmlNode["TermsID"].InnerText;
                    leadTime = xmlNode["LeadTime"].InnerText;
                    poDesc = xmlNode["Description"].InnerText;
                    orderDate = xmlNode["Date"].InnerText;
                    uom = xmlNode["UOM"].InnerText;
                    freight = xmlNode["Freight"].InnerText;
                    purchOfficer = xmlNode["PurchOfficer"].InnerText;
                    dueDateOri = xmlNode["DueDate"].InnerText;
                    dueSiteOri = xmlNode["DueSite"].InnerText;

                    if (purchOfficer == "" || purchOfficer == "ADMIN")
                    {
                        purchOfficer = "ADMIN";
                        position = "INTPO";
                    }

                    if (poDesc.Contains("&") == true)
                    {
                        poDesc = poDesc.Replace("&", "");
                    }

                    if (poDesc.Contains("#") == true)
                    {
                        poDesc = poDesc.Replace("#", "");
                    }

                    if (poDesc.Contains("\"") == true)
                    {
                        poDesc = poDesc.Replace("\"", "");
                    }

                    if (poDesc.Contains("<") == true)
                    {
                        poDesc = poDesc.Replace("<", "");
                    }

                    if (poDesc.Contains(">") == true)
                    {
                        poDesc = poDesc.Replace(">", "");
                    }

                    if (poDesc.Contains("'") == true)
                    {
                        poDesc = poDesc.Replace("'", "");
                    }

                    if (poDesc.Contains("≤") == true)
                    {
                        poDesc = poDesc.Replace("≤", "");
                    }

                    if (poDesc.Contains("≥") == true)
                    {
                        poDesc = poDesc.Replace("≥", "");
                    }

                    if (poDesc.Contains("%") == true)
                    {
                        poDesc = poDesc.Replace("%", "");
                    }

                    if (poDesc.Contains("+"))
                    {
                        poDesc = poDesc.Replace("+", "");
                    }

                    if (itemDesc.Contains("&") == true)
                    {
                        itemDesc = itemDesc.Replace("&", "");
                    }
                     
                    if (itemDesc.Contains("#") == true)
                    {
                        itemDesc = itemDesc.Replace("#", "");
                    }
                     
                    if (itemDesc.Contains("\"") == true)
                    {
                        itemDesc = itemDesc.Replace("\"", "");
                    }
                     
                    if (itemDesc.Contains("<") == true)
                    {
                        itemDesc = itemDesc.Replace("<", "");
                    }

                    if (itemDesc.Contains(">") == true)
                    {
                        itemDesc = itemDesc.Replace(">", "");
                    }

                    if (itemDesc.Contains("'") == true)
                    {
                        itemDesc = itemDesc.Replace("'", "");
                    }

                    if (itemDesc.Contains("≤") == true)
                    {
                        itemDesc = itemDesc.Replace("≤", "");
                    }

                    if (itemDesc.Contains("≥") == true)
                    {
                        itemDesc = itemDesc.Replace("≥", "");
                    }

                    if (itemDesc.Contains("%") == true)
                    {
                        itemDesc = itemDesc.Replace("%", "");
                    }

                    if (itemDesc.Contains("+"))
                    {
                        itemDesc = itemDesc.Replace("+", "");
                    }

                    string description1 = "";
                    string description2 = "";
                    string description3 = "";
                    string description4 = "";
                    string priority = "";
                    string dateRequired = "";
                    string dueSite = "";
                    string orderDates = "";
                    string dueDate = "";

                    if (promisedOn != null && promisedOn != "")
                    {
                        dateRequired = dueDateOri.Substring(0, 4) + dueDateOri.Substring(5, 2) + dueDateOri.Substring(8, 2);
                        dueSite = dueSiteOri.Substring(0, 4) + dueSiteOri.Substring(5, 2) + dueSiteOri.Substring(8, 2);
                    }
                    else
                    {
                        dateRequired = dueDateOri.Substring(0, 4) + dueDateOri.Substring(5, 2) + dueDateOri.Substring(8, 2);
                    }

                    if (orderDate != null && orderDate != "")
                    {
                        orderDates = orderDate.Substring(0, 4) + orderDate.Substring(5, 2) + orderDate.Substring(8, 2);
                    }
                    
                    if (dueDateOri != null && dueDateOri != "")
                    {
                        dueDate = dueDateOri.Substring(0, 4) + dueDateOri.Substring(5, 2) + dueDateOri.Substring(8, 2);
                    }

                    if (type == "Low")
                    {
                        priority = "0";
                    }
                    else if (type == "Normal")
                    {
                        priority = "1";
                    }
                    else if (type == "High")
                    {
                        priority = "2";
                    }

                    if (itemDesc.Length > 40 && itemDesc.Length <= 80)
                    {
                        description1 = itemDesc.Substring(0, 40);
                        description2 = itemDesc.Substring(40,itemDesc.Trim().Length - 40);
                    }
                    else if (itemDesc.Length > 80 && itemDesc.Length <= 120)
                    {
                        description1 = itemDesc.Substring(0, 40);
                        description2 = itemDesc.Substring(40, 40);
                        description3 = itemDesc.Substring(80, itemDesc.Trim().Length - 80);
                    }
                    else if (itemDesc.Length > 120)
                    {
                        description1 = itemDesc.Substring(0, 40);
                        description2 = itemDesc.Substring(40, 40);
                        description3 = itemDesc.Substring(80, 40);
                        if (itemDesc.Length > 120 && itemDesc.Length <= 160)
                        {
                            description4 = itemDesc.Substring(120, itemDesc.Trim().Length - 120);
                        }
                        else
                        {
                            description4 = itemDesc.Substring(120, 40);
                        }
                    }
                    else if (itemDesc.Length <= 40)
                    {
                        description1 = itemDesc.Trim();
                    }

                    hasilXml = "<?xml version='1.0' encoding='UTF-8'?>";
                    hasilXml = hasilXml + "<SyncCodeEntry " +
                     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                     "xmlns:ip='http://www.ventyx.com/IP/1' " +
                     "xsi:noNamespaceSchemaLocation='file:///C:/git-clones/mib/mib-common/schema/IP/1_0_0/BODs/SyncCodeEntry.xsd'>";
                    hasilXml = hasilXml + "<ApplicationArea>" +
                     "<Sender>" +
                     "<LogicalID>Testing</LogicalID><TaskID>SyncCodeEntry</TaskID>" +
                    "<ReferenceID>CodeEntry</ReferenceID>" +
                    "</Sender>" +
                    "<CreationDateTime>" + String.Format("{0:yyyy-MM-ddThh:mm:ss tt}", DateTime.Now) + "</CreationDateTime>" +
                    "<BODID>JUnit Test</BODID>" +
                    "<Extension>";
                    hasilXml = hasilXml + "<AnyExtension>" +
                    "<ServiceContext>" +
                    "<ServiceName>SyncCodeEntry</ServiceName>" +
                    "<ServiceVersion>1.0</ServiceVersion>" +
                    "<MessageType>CodeEntry</MessageType>" +
                    "</ServiceContext>";
                    hasilXml = hasilXml + "<AuthorizationContext>" +
                            "<Proxy>" +
                            "<UserID>INTEGRAT</UserID>" +
                            "<OrgID>SC01</OrgID>" +
                            "<Role>SYSADMIN</Role>" +
                            "<RunAsUserID>" + purchOfficer.Trim() + "</RunAsUserID>" +
                            "<RunAsOrgID>SC01</RunAsOrgID>" +
                            "<RunAsRole>INTPO</RunAsRole>" +
                            "<Password>P@ssw0rd2018</Password>" +
                            "</Proxy>" +
                            "</AuthorizationContext>";
                    hasilXml = hasilXml + "</AnyExtension>" +
                            "</Extension>" +
                            "</ApplicationArea>" +
                            "<DataArea>" +
                            "<Get />";
                    hasilXml = hasilXml + "<MSO230_OPT1Message>";
                    hasilXml = hasilXml + "<PREQ_NO>" + reqNbr.Trim() + "</PREQ_NO>";
                    hasilXml = hasilXml + "<PREQ_ITEM>" + lineNbr.Trim() + "</PREQ_ITEM>";
                    hasilXml = hasilXml + "<ACCOUNT_CODE>" + accountCode.Trim() + "</ACCOUNT_CODE>";
                    hasilXml = hasilXml + "<PRIORITY>" + priority.Trim() + "</PRIORITY>";
                    hasilXml = hasilXml + "<APPROVED>True</APPROVED>";
                    hasilXml = hasilXml + "<PO_NO>" + poNo.Trim() + "</PO_NO>";
                    hasilXml = hasilXml + "<DATE_REQUIRED>" + dateRequired.Trim() + "</DATE_REQUIRED>";
                    hasilXml = hasilXml + "<SUPPLIER_NO>" + supplierNo.Trim() + "</SUPPLIER_NO>";
                    hasilXml = hasilXml + "<PRICE_CODE>" + termsID.Trim() + "</PRICE_CODE>";
                    hasilXml = hasilXml + "<LEAD_TIME>" + leadTime.Trim() + "</LEAD_TIME>";
                    hasilXml = hasilXml + "<PO_DESCRIPTION1>" + poDesc.Trim() + "</PO_DESCRIPTION1>";
                    hasilXml = hasilXml + "<DESC_LINE1>" + description1.Trim() + "</DESC_LINE1>";
                    if (description2 == "") hasilXml = hasilXml + "<DESC_LINE2/>"; else hasilXml = hasilXml + "<DESC_LINE2>" + description2.Trim() + "</DESC_LINE2>";
                    if (description3 == "") hasilXml = hasilXml + "<DESC_LINE3/>"; else hasilXml = hasilXml + "<DESC_LINE3>" + description3.Trim() + "</DESC_LINE3>";
                    if (description4 == "") hasilXml = hasilXml + "<DESC_LINE4/>"; else hasilXml = hasilXml + "<DESC_LINE4>" + description4.Trim() + "</DESC_LINE4>";

                    if (itemType == "S")
                    {
                        unitCost = amount.Trim();
                    }

                    hasilXml = hasilXml + "<GROSS_PRICE>" + unitCost.Trim() + "</GROSS_PRICE>";
                    hasilXml = hasilXml + "<PURCH_OFFICER>" + purchOfficer.Trim() + "</PURCH_OFFICER>";
                    hasilXml = hasilXml + "<CURRENCY>" + currency.Trim() + "</CURRENCY>";
                    hasilXml = hasilXml + "<DELIVERY>01</DELIVERY>";
                    hasilXml = hasilXml + "<O_T></O_T>";
                    hasilXml = hasilXml + "<EXPEDITE>NM</EXPEDITE>";
                    hasilXml = hasilXml + "<FREIGHT>NA</FREIGHT>";
                    hasilXml = hasilXml + "<WAREHOUSE>MAIN</WAREHOUSE>";
                    hasilXml = hasilXml + "<ORDER_DATE>" + orderDates.Trim() + "</ORDER_DATE>";
                    hasilXml = hasilXml + "<DUE_DATE>" + dueDate.Trim() + "</DUE_DATE>";
                    hasilXml = hasilXml + "<DUE_SITE>" + dueDate.Trim() + "</DUE_SITE>";
                    hasilXml = hasilXml + "<PPN>00</PPN>";
                    hasilXml = hasilXml + "<PO_ITEM>" + lineNbr.Trim() + "</PO_ITEM>";
                    hasilXml = hasilXml + "<AUTH_BY>" + purchOfficer.Trim() + "</AUTH_BY>";
                    hasilXml = hasilXml + "<TERMS_ID>" + termsID.Trim() + "</TERMS_ID>";
                    hasilXml = hasilXml + "<FREIGHT>" + freight.Trim() + "</FREIGHT>";
                    hasilXml = hasilXml + "<TERMS_DESC></TERMS_DESC>";
                    hasilXml = hasilXml + "<TAX_CATEGORY>00</TAX_CATEGORY>";
                    hasilXml = hasilXml + "<DELIV_INST></DELIV_INST>";

                    if (projectNo == "")
                    {
                        hasilXml = hasilXml + "<WORK_OR_PROJ></WORK_OR_PROJ>";
                        hasilXml = hasilXml + "<WORK_OR_PROJ_IND></WORK_OR_PROJ_IND>";
                    }
                    else
                    {
                        hasilXml = hasilXml + "<WORK_OR_PROJ>" + projectNo + "</WORK_OR_PROJ>";
                        hasilXml = hasilXml + "<WORK_OR_PROJ_IND>P</WORK_OR_PROJ_IND>";
                    }

                    hasilXml = hasilXml + "<ITEM_TYPE>" + itemType.Trim() + "</ITEM_TYPE>";

                    if (itemType == "S")
                    {
                        orderQty = "";
                    }
                    
                    hasilXml = hasilXml + "<QUANTITY>" + orderQty.Trim() + "</QUANTITY>";

                    if (itemType == "S")
                    {
                        uom = "";
                    }

                    hasilXml = hasilXml + "<UOM>" + uom.Trim() + "</UOM>";
                    hasilXml = hasilXml + "<EST_PRICE>" + unitCost.Trim() + "</EST_PRICE>";
                    hasilXml = hasilXml + "<MNEMONIC></MNEMONIC>";
                    hasilXml = hasilXml + "<PART_NO></PART_NO>";
                    hasilXml = hasilXml + "</MSO230_OPT1Message>";
                    hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                    //await Task.Run(() =>
                    //{
                        result = PostMessage("queue://MIB.CUSTOM.GW.MSO230_OPT1", hasilXml);
                    //});
                }
            }

            if (jsonReq.Trim().Contains("ARS-Vendors").Equals(true))
            {
                XmlDocument xmlDoc = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq, "root");
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes("root/Inserted");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string supplierNo = "";
                    string supplierName = "";
                    string countryCode = "";
                    string addressLine1 = "";
                    string addressLine2 = "";
                    string city = "";
                    string postalCode = "";
                    string currencyid = "";
                    string phone = "";
                    string fax = "";
                    string email = "";
                    string contactPerson = "";

                    supplierNo = xmlNode["SupplierNo"].InnerText;
                    supplierName = xmlNode["SupplierName"].InnerText;
                    countryCode = xmlNode["CountryCode"].InnerText;
                    addressLine1 = xmlNode["AddressLine1"].InnerText;
                    addressLine2 = xmlNode["AddressLine2"].InnerText;
                    city = xmlNode["City"].InnerText;
                    postalCode = xmlNode["PostalCode"].InnerText;
                    currencyid = xmlNode["CurrencyID"].InnerText;
                    phone = xmlNode["Phone"].InnerText;
                    fax = xmlNode["Fax"].InnerText;
                    email = xmlNode["Email"].InnerText;
                    contactPerson = xmlNode["ContactPerson"].InnerText;

                    if (supplierName.Contains("&") == true)
                    {
                        supplierName = supplierName.Replace("&", "");
                    }

                    if (supplierName.Contains("#") == true)
                    {
                        supplierName = supplierName.Replace("#", "");
                    }

                    if (supplierName.Contains("\"") == true)
                    {
                        supplierName = supplierName.Replace("\"", "");
                    }

                    if (supplierName.Contains("<") == true)
                    {
                        supplierName = supplierName.Replace("<", "");
                    }

                    if (supplierName.Contains(">") == true)
                    {
                        supplierName = supplierName.Replace(">", "");
                    }

                    if (supplierName.Contains("'") == true)
                    {
                        supplierName = supplierName.Replace("'", "");
                    }

                    if (supplierName.Contains("≤") == true)
                    {
                        supplierName = supplierName.Replace("≤", "");
                    }

                    if (supplierName.Contains("≥") == true)
                    {
                        supplierName = supplierName.Replace("≥", "");
                    }

                    if (supplierName.Contains("%") == true)
                    {
                        supplierName = supplierName.Replace("%", "");
                    }

                    if (supplierName.Contains("+"))
                    {
                        supplierName = supplierName.Replace("+", "");
                    }

                    if (addressLine1.Length > 32)
                    {
                        addressLine1 = addressLine1.Substring(0, 32);
                    }

                    if (addressLine2.Length > 32)
                    {
                        addressLine2 = addressLine2.Substring(0, 32);
                    }

                    hasilXml = "<?xml version='1.0' encoding='UTF-8'?>";
                    hasilXml = hasilXml + "<SyncCodeEntry " +
                     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                     "xmlns:ip='http://www.ventyx.com/IP/1' " +
                     "xsi:noNamespaceSchemaLocation='file:///C:/git-clones/mib/mib-common/schema/IP/1_0_0/BODs/SyncCodeEntry.xsd'>";
                    hasilXml = hasilXml + "<ApplicationArea>" +
                     "<Sender>" +
                     "<LogicalID>Testing</LogicalID><TaskID>SyncCodeEntry</TaskID>" +
                    "<ReferenceID>CodeEntry</ReferenceID>" +
                    "</Sender>" +
                    "<CreationDateTime>" + String.Format("{0:yyyy-MM-ddThh:mm:ss tt}", DateTime.Now) + "</CreationDateTime>" +
                    "<BODID>JUnit Test</BODID>" +
                    "<Extension>";
                    hasilXml = hasilXml + "<AnyExtension>" +
                    "<ServiceContext>" +
                    "<ServiceName>SyncCodeEntry</ServiceName>" +
                    "<ServiceVersion>1.0</ServiceVersion>" +
                    "<MessageType>CodeEntry</MessageType>" +
                    "</ServiceContext>";
                    hasilXml = hasilXml + "<AuthorizationContext>" +
                            "<Proxy>" +
                            "<UserID>INTEGRAT</UserID>" +
                            "<OrgID>SC01</OrgID>" +
                            "<Role>SYSADMIN</Role>" +
                            "<RunAsUserID>ADMIN</RunAsUserID>" +
                            "<RunAsOrgID>SC01</RunAsOrgID>" +
                            "<RunAsRole>INTPO</RunAsRole>" +
                            "<Password>P@ssw0rd2018</Password>" +
                            "</Proxy>" +
                            "</AuthorizationContext>";
                    hasilXml = hasilXml + "</AnyExtension>" +
                            "</Extension>" +
                            "</ApplicationArea>" +
                            "<DataArea>" +
                            "<Get />";
                    hasilXml = hasilXml + "<MSO200_OPT1Message>";
                    hasilXml = hasilXml + "<SUPPLIER_CODE>" + supplierNo + "</SUPPLIER_CODE>";
                    hasilXml = hasilXml + "<SUPPLIER_STATUS>N</SUPPLIER_STATUS>";
                    hasilXml = hasilXml + "<COUNTRY_CODE>" + countryCode + "</COUNTRY_CODE>";
                    hasilXml = hasilXml + "<SUPPLIER_NAME>" + supplierName + "</SUPPLIER_NAME>";
                    hasilXml = hasilXml + "<ORDER_ADDRESS1>" + addressLine1 + "</ORDER_ADDRESS1>";
                    hasilXml = hasilXml + "<ORDER_ADDRESS2>" + addressLine2 + "</ORDER_ADDRESS2>";
                    hasilXml = hasilXml + "<CITY>" + city + "</CITY>";
                    hasilXml = hasilXml + "<POSTAL_CODE>" + postalCode + "</POSTAL_CODE>";
                    hasilXml = hasilXml + "<CLOSED_FORXMAS>N</CLOSED_FORXMAS>";
                    hasilXml = hasilXml + "<CURRENCY_TYPE>" + currencyid + "</CURRENCY_TYPE>";
                    hasilXml = hasilXml + "<ITEM_PERORDER>M</ITEM_PERORDER>";
                    hasilXml = hasilXml + "<INV_IND>I</INV_IND>";
                    hasilXml = hasilXml + "<NO_OF_PAYS>30</NO_OF_PAYS>";
                    hasilXml = hasilXml + "<MAX_ITEMS>999</MAX_ITEMS>";
                    hasilXml = hasilXml + "<PAYMENT_METHOD>C</PAYMENT_METHOD>";
                    hasilXml = hasilXml + "<ORDER_ALLOWED>Y</ORDER_ALLOWED>";
                    hasilXml = hasilXml + "<PAYMENT_ALLOWED>Y</PAYMENT_ALLOWED>";
                    hasilXml = hasilXml + "<BANK_RECONCILE>N</BANK_RECONCILE>";
                    hasilXml = hasilXml + "<PHONE>" + phone + "</PHONE>";
                    hasilXml = hasilXml + "<FAX>" + fax + "</FAX>";
                    hasilXml = hasilXml + "<EMAIL>" + email + "</EMAIL>";
                    hasilXml = hasilXml + "<CONTACT>" + contactPerson + "</CONTACT>";
                    hasilXml = hasilXml + "</MSO200_OPT1Message>";
                    hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                    await Task.Run(() =>
                    {
                        result = PostMessage("queue://MIB.CUSTOM.GW.MSO200_OPT1", hasilXml);
                    });
                }
            }

            if (jsonReq.Trim().Contains("ARS-Shipment").Equals(true))
            {
                XmlDocument xmlDoc = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq, "root");
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes("root/Inserted");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string indikator = "J";
                    string shipmentNbr = "";
                    string pONbr = "";
                    string pOItem = "";
                    string pOCurrency = "";
                    string pOReceivedAmount = "";
                    string pOLocalAmount = "";
                    string pOOriginalAmount = "";
                    string sONbr = "";
                    string sOItem = "";
                    string sOCurrency = "";
                    string accountPeriod = "";
                    string tranDate = "";
                    string journalApprover = "";
                    string supplierNo = "";
                    string customerNo = "";
                    string invtAcct = "";
                    string cogsAcct = "";
                    string custSalesAcct = "";
                    string shipmentDate = "";
                    string custSalesSub = "";
                    string invtSub = "";
                    string cogsSub = "";
                    string shipLineType = "";
                    string purchMethod = "";
                    string sOCuryExtPrice = "";
                    string sOExtPrice = "";
                    string aRPendingAcct = "";

                    shipmentNbr = xmlNode["ShipmentNbr"].InnerText;
                    pONbr = xmlNode["PONbr"].InnerText;
                    pOItem = xmlNode["POLineNbr"].InnerText;
                    pOCurrency = xmlNode["POCurreny"].InnerText;
                    pOLocalAmount = xmlNode["POReceivedCost"].InnerText;
                    pOOriginalAmount = xmlNode["POCuryReceivedCost"].InnerText;

                    if (pOCurrency == "IDR")
                    {
                        pOReceivedAmount = xmlNode["POReceivedCost"].InnerText;
                    }
                    else
                    {
                        pOReceivedAmount = xmlNode["POCuryReceivedCost"].InnerText;
                    }

                    sONbr = xmlNode["SONbr"].InnerText;
                    sOItem = xmlNode["SOLineNbr"].InnerText;
                    sOCurrency = xmlNode["SOCurrency"].InnerText;
                    accountPeriod = String.Format("{0:MM/yy}", DateTime.Today);
                    shipmentDate = xmlNode["ShipmentDate"].InnerText;
                    tranDate = shipmentDate.Substring(0, 4) + shipmentDate.Substring(5, 2) + shipmentDate.Substring(8, 2);
                    journalApprover = "8208018JA";
                    supplierNo = xmlNode["SupplierNo"].InnerText;
                    customerNo = xmlNode["CustomerID"].InnerText;
                    invtAcct = xmlNode["InvtAcct"].InnerText;
                    invtSub = xmlNode["InvtSub"].InnerText;
                    cogsAcct = xmlNode["CogsAcct"].InnerText;
                    cogsSub = xmlNode["CogsSub"].InnerText;
                    custSalesAcct = xmlNode["CustSalesAccount"].InnerText;
                    custSalesSub = xmlNode["CustSalesSub"].InnerText;
                    shipLineType = xmlNode["ShipLineType"].InnerText;
                    purchMethod = xmlNode["PurchMethod"].InnerText;
                    aRPendingAcct = xmlNode["ARPendingAcct"].InnerText;
                    sOCuryExtPrice = xmlNode["SOCuryExtPrice"].InnerText;
                    sOExtPrice = xmlNode["SOExtPrice"].InnerText;

                    hasilXml = "<?xml version='1.0' encoding='UTF-8'?>";
                    hasilXml = hasilXml + "<SyncCodeEntry " +
                     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                     "xmlns:ip='http://www.ventyx.com/IP/1' " +
                     "xsi:noNamespaceSchemaLocation='file:///C:/git-clones/mib/mib-common/schema/IP/1_0_0/BODs/SyncCodeEntry.xsd'>";
                    hasilXml = hasilXml + "<ApplicationArea>" +
                     "<Sender>" +
                     "<LogicalID>Testing</LogicalID><TaskID>SyncCodeEntry</TaskID>" +
                    "<ReferenceID>CodeEntry</ReferenceID>" +
                    "</Sender>" +
                    "<CreationDateTime>" + String.Format("{0:yyyy-MM-ddThh:mm:ss tt}", DateTime.Now) + "</CreationDateTime>" +
                    "<BODID>JUnit Test</BODID>" +
                    "<Extension>";
                    hasilXml = hasilXml + "<AnyExtension>" +
                    "<ServiceContext>" +
                    "<ServiceName>SyncCodeEntry</ServiceName>" +
                    "<ServiceVersion>1.0</ServiceVersion>" +
                    "<MessageType>CodeEntry</MessageType>" +
                    "</ServiceContext>";
                    hasilXml = hasilXml + "<AuthorizationContext>" +
                            "<Proxy>" +
                            "<UserID>INTEGRAT</UserID>" +
                            "<OrgID>SC01</OrgID>" +
                            "<Role>SYSADMIN</Role>" +
                            "<RunAsUserID>ADMIN</RunAsUserID>" +
                            "<RunAsOrgID>SC01</RunAsOrgID>" +
                            "<RunAsRole>INTPO</RunAsRole>" +
                            "<Password>P@ssw0rd2018</Password>" +
                            "</Proxy>" +
                            "</AuthorizationContext>";
                    hasilXml = hasilXml + "</AnyExtension>" +
                            "</Extension>" +
                            "</ApplicationArea>" +
                            "<DataArea>" +
                            "<Get />";

                    hasilXml = hasilXml + "<MSO905_OPT3Message>";
                    hasilXml = hasilXml + "<MAN_JNL_NO1I>" + indikator + shipmentNbr.Trim() + pOItem.ToString() + "</MAN_JNL_NO1I>";

                    hasilXml = hasilXml + "<ACCT_PERIOD1I>" + accountPeriod + "</ACCT_PERIOD1I>";
                    hasilXml = hasilXml + "<INTERDIST_IND1I></INTERDIST_IND1I>";
                    hasilXml = hasilXml + "<PO_CURRENCY>" + pOCurrency + "</PO_CURRENCY>";
                    hasilXml = hasilXml + "<PO_NO>" + pONbr + "</PO_NO>";
                    hasilXml = hasilXml + "<PO_ITEM_NO>" + pOItem + "</PO_ITEM_NO>";

                    if (pOCurrency.Trim() == "IDR")
                    {
                        pOReceivedAmount = pOLocalAmount.ToString();
                    }
                    else
                    {
                        pOReceivedAmount = pOOriginalAmount.ToString();
                    }
                    hasilXml = hasilXml + "<RECEIPTTOTAL>" + pOReceivedAmount + "</RECEIPTTOTAL>";

                    if (pOCurrency.Trim() != "IDR")
                    {
                        hasilXml = hasilXml + "<FOREIGN_IND1I>Y</FOREIGN_IND1I>";
                    }
                    else
                    {
                        hasilXml = hasilXml + "<FOREIGN_IND1I></FOREIGN_IND1I>";
                    }
                    hasilXml = hasilXml + "<JOURNAL_DESC1I> Shipment DO-" + sONbr.Trim() + " item ke-" + sOItem.Trim() + "</JOURNAL_DESC1I>";
                    hasilXml = hasilXml + "<ACCOUNTANT1I>ADMIN</ACCOUNTANT1I>";
                    hasilXml = hasilXml + "<JNL_IND1I></JNL_IND1I>";

                    hasilXml = hasilXml + "<TRANS_DATE1I>" + tranDate + "</TRANS_DATE1I>";
                    hasilXml = hasilXml + "<HDR_AUTH_BY1I >" + journalApprover + "</HDR_AUTH_BY1I >";
                    hasilXml = hasilXml + "<ITEM>";

                    if (shipLineType.Trim() == "GI")
                    {
                        hasilXml = hasilXml + "<JNL_DESC1I>HPP " + sONbr.Trim() + ",item " + sOItem.Trim() + ", " + supplierNo.Trim() + ", " + pONbr.Trim() + "</JNL_DESC1I>";
                        hasilXml = hasilXml + "<DSTRCT_CODE1I>SC01</DSTRCT_CODE1I>";
                        hasilXml = hasilXml + "<ACCOUNT_CODE1I>" + custSalesSub.Trim() + cogsAcct.Trim() + "</ACCOUNT_CODE1I>";
                        hasilXml = hasilXml + "<WORK_PROJ1I>" + sONbr.Trim() + "</WORK_PROJ1I>";
                        hasilXml = hasilXml + "<WORK_PROJ_IND1I>P</WORK_PROJ_IND1I>";

                        if (pOCurrency.Trim() != "IDR")
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I>" + pOCurrency.Trim() + "</FOREIGN_CURR1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I></FOREIGN_CURR1I>";
                        }

                        if (pOCurrency.Trim() != "IDR")
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I>" + pOReceivedAmount.Trim() + "</MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I></TRAN_AMOUNT1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I></MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I>" + pOReceivedAmount.Trim() + "</TRAN_AMOUNT1I>";
                        }
                        hasilXml = hasilXml + "<PLANT_NO1I></PLANT_NO1I>";
                        hasilXml = hasilXml + "<DOCUMENT_REF1I></DOCUMENT_REF1I>";

                        hasilXml = hasilXml + "<JNL_DESC1I>PRSD " + sONbr.Trim() + ",item " + sOItem.Trim() + ", " + supplierNo.Trim() + ", " + pONbr.Trim() + "</JNL_DESC1I>";
                        hasilXml = hasilXml + "<DSTRCT_CODE1I>SC01</DSTRCT_CODE1I>";
                        hasilXml = hasilXml + "<ACCOUNT_CODE1I>" + invtAcct.Trim() + "</ACCOUNT_CODE1I>";
                        hasilXml = hasilXml + "<WORK_PROJ1I>" + sONbr.Trim() + "</WORK_PROJ1I>";
                        hasilXml = hasilXml + "<WORK_PROJ_IND1I>P</WORK_PROJ_IND1I>";

                        if (pOCurrency.Trim() != "IDR")
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I>" + pOCurrency.Trim() + "</FOREIGN_CURR1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I></FOREIGN_CURR1I>";
                        }

                        if (pOCurrency.Trim() != "IDR")
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I>-" + pOReceivedAmount.Trim() + "</MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I></TRAN_AMOUNT1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I></MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I>-" + pOReceivedAmount.Trim() + "</TRAN_AMOUNT1I>";
                        }

                        hasilXml = hasilXml + "<PLANT_NO1I></PLANT_NO1I>";
                        hasilXml = hasilXml + "<DOCUMENT_REF1I></DOCUMENT_REF1I>";
                    }

                    if (purchMethod == "B2B")
                    {
                        hasilXml = hasilXml + "<JNL_DESC1I>ARP " + sONbr.Trim() + ",item " + sONbr.Trim() + ", " + supplierNo.Trim() + ", " + pONbr.Trim() + "</JNL_DESC1I>";
                        hasilXml = hasilXml + "<DSTRCT_CODE1I>SC01</DSTRCT_CODE1I>";
                        hasilXml = hasilXml + "<ACCOUNT_CODE1I>" + aRPendingAcct.Trim() + "</ACCOUNT_CODE1I>";
                        hasilXml = hasilXml + "<WORK_PROJ1I>" + sONbr.Trim() + "</WORK_PROJ1I>";
                        hasilXml = hasilXml + "<WORK_PROJ_IND1I>P</WORK_PROJ_IND1I>";

                        if (sOCurrency != "IDR")
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I>" + sOCurrency.Trim() + "</FOREIGN_CURR1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I></FOREIGN_CURR1I>";
                        }

                        if (sOCurrency.Trim() != "IDR")
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I>" + sOCuryExtPrice.Trim() + "</MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I></TRAN_AMOUNT1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I></MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I>" + sOExtPrice.Trim() + "</TRAN_AMOUNT1I>";
                        }

                        hasilXml = hasilXml + "<PLANT_NO1I></PLANT_NO1I>";
                        hasilXml = hasilXml + "<DOCUMENT_REF1I></DOCUMENT_REF1I>";

                        hasilXml = hasilXml + "<JNL_DESC1I>Sales " + sONbr.Trim() + ",item " + sOItem.Trim() + ", " + supplierNo.Trim() + ", " + pONbr.Trim() + "</JNL_DESC1I>";
                        hasilXml = hasilXml + "<DSTRCT_CODE1I>SC01</DSTRCT_CODE1I>";
                        hasilXml = hasilXml + "<ACCOUNT_CODE1I>" + custSalesSub.Trim() + custSalesAcct.Trim() + "</ACCOUNT_CODE1I>";
                        hasilXml = hasilXml + "<WORK_PROJ1I>" + sONbr.Trim() + "</WORK_PROJ1I>";
                        hasilXml = hasilXml + "<WORK_PROJ_IND1I>P</WORK_PROJ_IND1I>";

                        if (sOCurrency.Trim() != "IDR")
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I>" + sOCurrency.Trim() + "</FOREIGN_CURR1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<FOREIGN_CURR1I></FOREIGN_CURR1I>";
                        }

                        if (sOCurrency.Trim() != "IDR")
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I>-" + sOCuryExtPrice.Trim() + "</MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I></TRAN_AMOUNT1I>";
                        }
                        else
                        {
                            hasilXml = hasilXml + "<MEMO_AMOUNT1I></MEMO_AMOUNT1I>";
                            hasilXml = hasilXml + "<TRAN_AMOUNT1I>-" + sOCuryExtPrice.Trim() + "</TRAN_AMOUNT1I>";
                        }

                        hasilXml = hasilXml + "<PLANT_NO1I></PLANT_NO1I>";
                        hasilXml = hasilXml + "<DOCUMENT_REF1I></DOCUMENT_REF1I>";
                    }
                    hasilXml = hasilXml + "</ITEM>";
                    hasilXml = hasilXml + "</MSO905_OPT3Message>";
                    hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                    await Task.Run(() =>
                    {
                        result = PostMessage("queue://MIB.CUSTOM.GW.MSO905_OPT3", hasilXml);
                    });
                }
            }

            if (jsonReq.Trim().Contains("ARS-SalesOrder").Equals(true))
            {
                XmlDocument xmlDoc = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq, "root");
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes("root/Inserted");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string projectNo = "";
                    string projectDesc = "";
                    string originator = "";
                    string accountCode = "";
                    string currencyID = "";
                    string estCost = "";
                    string customerNo = "";
                    string actStartDate = "";
                    string planFinDate = "";
                    string asd = "";
                    string pfd = "";
                    string externalDO = "";

                    projectNo = xmlNode["ProjectNo"].InnerText.Trim();
                    projectDesc = xmlNode["ProjectDesc"].InnerText.Trim();
                    originator = xmlNode["OriginatorID"].InnerText.Trim();
                    accountCode = xmlNode["Account"].InnerText.Trim();
                    currencyID = xmlNode["CurrencyID"].InnerText.Trim();

                    if (currencyID != "IDR")
                    {
                        estCost = xmlNode["EstAmountOrig"].InnerText.Trim();
                    }
                    else
                    {
                        estCost = xmlNode["EstAmountLoc"].InnerText.Trim();
                    }

                    customerNo = xmlNode["CustomerNo"].InnerText.Trim();
                    asd = xmlNode["ActStartDate"].InnerText.Trim();
                    actStartDate = asd.Substring(0, 4) + asd.Substring(5, 2) + asd.Substring(8, 2);
                    pfd = xmlNode["PlanFinDate"].InnerText.Trim();
                    planFinDate = pfd.Substring(0, 4) + pfd.Substring(5, 2) + pfd.Substring(8, 2);
                    externalDO = xmlNode["ExternalDO"].InnerText.Trim();

                    if (projectDesc.Contains("&") == true)
                    {
                        projectDesc = projectDesc.Replace("&", "");
                    }

                    if (projectDesc.Contains("#") == true)
                    {
                        projectDesc = projectDesc.Replace("#", "");
                    }

                    if (projectDesc.Contains("\"") == true)
                    {
                        projectDesc = projectDesc.Replace("\"", "");
                    }

                    if (projectDesc.Contains("<") == true)
                    {
                        projectDesc = projectDesc.Replace("<", "");
                    }

                    if (projectDesc.Contains(">") == true)
                    {
                        projectDesc = projectDesc.Replace(">", "");
                    }

                    if (projectDesc.Contains("'") == true)
                    {
                        projectDesc = projectDesc.Replace("'", "");
                    }

                    if (projectDesc.Contains("≤") == true)
                    {
                        projectDesc = projectDesc.Replace("≤", "");
                    }

                    if (projectDesc.Contains("≥") == true)
                    {
                        projectDesc = projectDesc.Replace("≥", "");
                    }

                    if (projectDesc.Contains("%") == true)
                    {
                        projectDesc = projectDesc.Replace("%", "");
                    }

                    if (projectDesc.Contains("+"))
                    {
                        projectDesc = projectDesc.Replace("+", "");
                    }

                    hasilXml = "<?xml version='1.0' encoding='UTF-8'?>";
                    hasilXml = hasilXml + "<SyncCodeEntry " +
                     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                     "xmlns:ip='http://www.ventyx.com/IP/1' " +
                     "xsi:noNamespaceSchemaLocation='file:///C:/git-clones/mib/mib-common/schema/IP/1_0_0/BODs/SyncCodeEntry.xsd'>";
                    hasilXml = hasilXml + "<ApplicationArea>" +
                     "<Sender>" +
                     "<LogicalID>Testing</LogicalID><TaskID>SyncCodeEntry</TaskID>" +
                    "<ReferenceID>CodeEntry</ReferenceID>" +
                    "</Sender>" +
                    "<CreationDateTime>" + String.Format("{0:yyyy-MM-ddThh:mm:ss tt}", DateTime.Now) + "</CreationDateTime>" +
                    "<BODID>JUnit Test</BODID>" +
                    "<Extension>";
                    hasilXml = hasilXml + "<AnyExtension>" +
                    "<ServiceContext>" +
                    "<ServiceName>SyncCodeEntry</ServiceName>" +
                    "<ServiceVersion>1.0</ServiceVersion>" +
                    "<MessageType>CodeEntry</MessageType>" +
                    "</ServiceContext>";
                    hasilXml = hasilXml + "<AuthorizationContext>" +
                            "<Proxy>" +
                            "<UserID>INTEGRAT</UserID>" +
                            "<OrgID>SC01</OrgID>" +
                            "<Role>SYSADMIN</Role>" +
                            "<RunAsUserID>ADMIN</RunAsUserID>" +
                            "<RunAsOrgID>SC01</RunAsOrgID>" +
                            "<RunAsRole>INTPO</RunAsRole>" +
                            "<Password>P@ssw0rd2018</Password>" +
                            "</Proxy>" +
                            "</AuthorizationContext>";
                    hasilXml = hasilXml + "</AnyExtension>" +
                            "</Extension>" +
                            "</ApplicationArea>" +
                            "<DataArea>" +
                            "<Get />";

                    hasilXml = hasilXml + "<MSE660_OPTCMessage>";
                    hasilXml = hasilXml + "<projectNo>" + projectNo + "</projectNo>";
                    if (projectDesc == "") projectDesc = projectNo;
                    hasilXml = hasilXml + "<projDesc>" + projectDesc + "</projDesc>";
                    hasilXml = hasilXml + "<districtCode>" + "SC01" + "</districtCode>";
                    hasilXml = hasilXml + "<originatorId>" + originator + "</originatorId>";
                    hasilXml = hasilXml + "<accountCode>" + accountCode + "</accountCode>";
                    hasilXml = hasilXml + "<estimateCost>" + estCost + "</estimateCost>";
                    hasilXml = hasilXml + "<externalDO>" + externalDO + "</externalDO>";
                    hasilXml = hasilXml + "<actStartDate>" + actStartDate + "</actStartDate>";
                    hasilXml = hasilXml + "<CustomerNo>" + customerNo + "</CustomerNo>";
                    hasilXml = hasilXml + "<PlanFinishDate>" + planFinDate + "</PlanFinishDate>";
                    hasilXml = hasilXml + "</MSE660_OPTCMessage>";
                    hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                    await Task.Run(() =>
                    {
                        result = PostMessage("queue://MIB.CUSTOM.GW.MSE660_OPTC", hasilXml);
                    });
                }
            }

            if (jsonReq.Trim().Contains("ARS-Customers").Equals(true))
            {

                XmlDocument xmlDoc = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq, "root");
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes("root/Inserted");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string customerId = "";
                    string customerName = "";
                    string currencyId = "";
                    string country = "";
                    string addressLine1 = "";
                    string addressLine2 = "";
                    string city = "";
                    string postalCode = "";
                    string phone = "";
                    string fax = "";
                    string email = "";
                    string contactPerson = "";

                    customerId = xmlNode["CustomerCode"].InnerText;
                    customerName = xmlNode["CustomerName"].InnerText;
                    currencyId = xmlNode["CurrencyID"].InnerText;
                    country = xmlNode["Country"].InnerText;
                    addressLine1 = xmlNode["AddressLine1"].InnerText;
                    addressLine2 = xmlNode["AddressLine2"].InnerText;
                    city = xmlNode["City"].InnerText;
                    postalCode = xmlNode["PostalCode"].InnerText;
                    phone = xmlNode["Phone"].InnerText;
                    fax = xmlNode["Fax"].InnerText;
                    email = xmlNode["Email"].InnerText;
                    contactPerson = xmlNode["ContactPerson"].InnerText;

                    if (customerName.Contains("&") == true)
                    {
                        customerName = customerName.Replace("&", "");
                    }

                    if (customerName.Contains("#") == true)
                    {
                        customerName = customerName.Replace("#", "");
                    }

                    if (customerName.Contains("\"") == true)
                    {
                        customerName = customerName.Replace("\"", "");
                    }

                    if (customerName.Contains("<") == true)
                    {
                        customerName = customerName.Replace("<", "");
                    }

                    if (customerName.Contains(">") == true)
                    {
                        customerName = customerName.Replace(">", "");
                    }

                    if (customerName.Contains("'") == true)
                    {
                        customerName = customerName.Replace("'", "");
                    }

                    if (customerName.Contains("≤") == true)
                    {
                        customerName = customerName.Replace("≤", "");
                    }

                    if (customerName.Contains("≥") == true)
                    {
                        customerName = customerName.Replace("≥", "");
                    }

                    if (customerName.Contains("%") == true)
                    {
                        customerName = customerName.Replace("%", "");
                    }

                    if (customerName.Contains("+"))
                    {
                        customerName = customerName.Replace("+", "");
                    }

                    if (addressLine1.Length > 32)
                    {
                        addressLine1 = addressLine1.Substring(0, 32);
                    }

                    if (addressLine2.Length > 32)
                    {
                        addressLine2 = addressLine2.Substring(0, 32);
                    }

                    hasilXml = "<?xml version='1.0' encoding='UTF-8'?>";
                    hasilXml = hasilXml + "<SyncCodeEntry " +
                     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                     "xmlns:ip='http://www.ventyx.com/IP/1' " +
                     "xsi:noNamespaceSchemaLocation='file:///C:/git-clones/mib/mib-common/schema/IP/1_0_0/BODs/SyncCodeEntry.xsd'>";
                    hasilXml = hasilXml + "<ApplicationArea>" +
                     "<Sender>" +
                     "<LogicalID>Testing</LogicalID><TaskID>SyncCodeEntry</TaskID>" +
                    "<ReferenceID>CodeEntry</ReferenceID>" +
                    "</Sender>" +
                    "<CreationDateTime>" + String.Format("{0:yyyy-MM-ddThh:mm:ss tt}", DateTime.Now) + "</CreationDateTime>" +
                    "<BODID>JUnit Test</BODID>" +
                    "<Extension>";
                    hasilXml = hasilXml + "<AnyExtension>" +
                    "<ServiceContext>" +
                    "<ServiceName>SyncCodeEntry</ServiceName>" +
                    "<ServiceVersion>1.0</ServiceVersion>" +
                    "<MessageType>CodeEntry</MessageType>" +
                    "</ServiceContext>";
                    hasilXml = hasilXml + "<AuthorizationContext>" +
                            "<Proxy>" +
                            "<UserID>INTEGRAT</UserID>" +
                            "<OrgID>SC01</OrgID>" +
                            "<Role>SYSADMIN</Role>" +
                            "<RunAsUserID>ADMIN</RunAsUserID>" +
                            "<RunAsOrgID>SC01</RunAsOrgID>" +
                            "<RunAsRole>INTPO</RunAsRole>" +
                            "<Password>P@ssw0rd2018</Password>" +
                            "</Proxy>" +
                            "</AuthorizationContext>";
                    hasilXml = hasilXml + "</AnyExtension>" +
                            "</Extension>" +
                            "</ApplicationArea>" +
                            "<DataArea>" +
                            "<Get />";
                    hasilXml = hasilXml + "<MSO500_OPT1Message>";
                    hasilXml = hasilXml + "<CustomerCode>" + customerId + "</CustomerCode>";
                    hasilXml = hasilXml + "<District>SC01</District>";
                    hasilXml = hasilXml + "<CustomerName>" + customerName.Trim() + "</CustomerName>";
                    hasilXml = hasilXml + "<Currency>" + currencyId + "</Currency>";
                    hasilXml = hasilXml + "<Country>" + country + "</Country>";
                    hasilXml = hasilXml + "<CashDistrict></CashDistrict>";
                    hasilXml = hasilXml + "<Address1>" + addressLine1 + "</Address1>";
                    hasilXml = hasilXml + "<Address2>" + addressLine2 + "</Address2>";
                    hasilXml = hasilXml + "<Address3>" + city + "</Address3>";
                    hasilXml = hasilXml + "<PostCode>" + postalCode + "</PostCode>";
                    hasilXml = hasilXml + "<Contact>" + contactPerson + "</Contact>";
                    hasilXml = hasilXml + "<Phone>" + phone + "</Phone>";
                    hasilXml = hasilXml + "<TelexName></TelexName>";
                    hasilXml = hasilXml + "<TelexNo></TelexNo>";
                    hasilXml = hasilXml + "<Fax>" + fax + "</Fax>";
                    hasilXml = hasilXml + "<DeliveryAddress1>" + addressLine1 + "</DeliveryAddress1>";
                    hasilXml = hasilXml + "<DeliveryAddress2>" + addressLine2 + "</DeliveryAddress2>";
                    hasilXml = hasilXml + "<CustomerType></CustomerType>";
                    hasilXml = hasilXml + "<EmailAddress>" + email + "</EmailAddress>";
                    hasilXml = hasilXml + "<DeliveryLocation>" + city + "</DeliveryLocation>";
                    hasilXml = hasilXml + "<PaymentMethod></PaymentMethod>";
                    hasilXml = hasilXml + "<GovernIdNumber></GovernIdNumber>";
                    hasilXml = hasilXml + "<RegistrationNo></RegistrationNo>";
                    hasilXml = hasilXml + "<CreditManager></CreditManager>";
                    hasilXml = hasilXml + "<StatementType>O</StatementType>";
                    hasilXml = hasilXml + "<DaysToPay>30</DaysToPay>";
                    hasilXml = hasilXml + "</MSO500_OPT1Message>";
                    hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                    await Task.Run(() =>
                    {
                        result = PostMessage("queue://MIB.CUSTOM.GW.MSO500_OPT1", hasilXml);
                    });
                }
            }

            if (jsonReq.Trim().Contains("ARS-Receipt").Equals(true))
            {
                XmlDocument xmlDoc = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonReq, "root");
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes("root/Inserted");

                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    string poNo = "";
                    string poItemNo = "";
                    string poItemType = "";
                    string receiptNo = "";
                    string receiptRef = "";
                    string receiptDate = "";
                    string receiptType = "";
                    string receiptQty = "";
                    string orderQty = "";
                    string unitCost = "";
                    string extCost = "";

                    poNo = xmlNode["PoNo"].InnerText;
                    poItemNo = xmlNode["PoItemNo"].InnerText;
                    poItemType = xmlNode["PoItemType"].InnerText;
                    receiptNo = xmlNode["ReceiptNo"].InnerText;
                    receiptType = xmlNode["ReceiptType"].InnerText;
                    receiptDate = xmlNode["ReceiptDate"].InnerText;
                    receiptQty = xmlNode["ReceiptQty"].InnerText;
                    orderQty = xmlNode["OrderQty"].InnerText;
                    unitCost = xmlNode["UnitCost"].InnerText;
                    extCost = xmlNode["ExtCost"].InnerText;

                    if (poItemType == "GI")
                    {
                        //Stock
                        if (receiptType == "RT")
                        {
                            receiptRef = "G-" + receiptNo.Trim() + "RC";
                        }
                        else
                        {
                            receiptRef = "G-" + receiptNo.Trim() + "RT";
                        }
                    }
                    else if (poItemType != "GI")
                    {
                        // non stock

                        if (receiptType == "RT")
                        {
                            receiptRef = "S-" + receiptNo.Trim() + "RC";
                        }
                        else
                        {
                            receiptRef = "S-" + receiptNo.Trim() + "RT";
                        }
                    }

                    hasilXml = "<?xml version='1.0' encoding='UTF-8'?>";
                    hasilXml = hasilXml + "<SyncCodeEntry " +
                     "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                     "xmlns:ip='http://www.ventyx.com/IP/1' " +
                     "xsi:noNamespaceSchemaLocation='file:///C:/git-clones/mib/mib-common/schema/IP/1_0_0/BODs/SyncCodeEntry.xsd'>";
                    hasilXml = hasilXml + "<ApplicationArea>" +
                     "<Sender>" +
                     "<LogicalID>Testing</LogicalID><TaskID>SyncCodeEntry</TaskID>" +
                    "<ReferenceID>CodeEntry</ReferenceID>" +
                    "</Sender>" +
                    "<CreationDateTime>" + String.Format("{0:yyyy-MM-ddThh:mm:ss tt}", DateTime.Now) + "</CreationDateTime>" +
                    "<BODID>JUnit Test</BODID>" +
                    "<Extension>";
                    hasilXml = hasilXml + "<AnyExtension>" +
                    "<ServiceContext>" +
                    "<ServiceName>SyncCodeEntry</ServiceName>" +
                    "<ServiceVersion>1.0</ServiceVersion>" +
                    "<MessageType>CodeEntry</MessageType>" +
                    "</ServiceContext>";
                    hasilXml = hasilXml + "<AuthorizationContext>" +
                            "<Proxy>" +
                            "<UserID>INTEGRAT</UserID>" +
                            "<OrgID>SC01</OrgID>" +
                            "<Role>SYSADMIN</Role>" +
                            "<RunAsUserID>ADMIN</RunAsUserID>" +
                            "<RunAsOrgID>SC01</RunAsOrgID>" +
                            "<RunAsRole>INTPO</RunAsRole>" +
                            "<Password>P@ssw0rd2018</Password>" +
                            "</Proxy>" +
                            "</AuthorizationContext>";
                    hasilXml = hasilXml + "</AnyExtension>" +
                            "</Extension>" +
                            "</ApplicationArea>" +
                            "<DataArea>" +
                            "<Get />";
                    if (poItemType != "GI")
                    {
                        if (receiptType == "RT")
                        {
                            hasilXml = hasilXml + "<MSO155_OPT2Message>";
                            hasilXml = hasilXml + "<PO_NO>" + poNo.Trim() + "</PO_NO>";
                            hasilXml = hasilXml + "<PO_ITEM>" + poItemNo + "</PO_ITEM>";
                            hasilXml = hasilXml + "<RECEIPT_REF>" + receiptRef.Trim() + "</RECEIPT_REF>";
                            hasilXml = hasilXml + "<RECEIPT_ALL>Y</RECEIPT_ALL>";
                            hasilXml = hasilXml + "<RECEIPT_DATE>" + receiptDate + "</RECEIPT_DATE>";
                            hasilXml = hasilXml + "<VALUE_RCVD>" + extCost + "</VALUE_RCVD>";
                            hasilXml = hasilXml + "<ORDER_VALUE>" + extCost + "</ORDER_VALUE>";
                            hasilXml = hasilXml + "</MSO155_OPT2Message>";
                            hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                            await Task.Run(() =>
                            {
                                result = PostMessage("queue://MIB.CUSTOM.GW.MSO155_OPT2", hasilXml);
                            });
                        }
                        else
                        {
                            hasilXml = hasilXml + "<MSO155_OPT3Message>";
                            hasilXml = hasilXml + "<PO_NO>" + poNo.Trim() + "</PO_NO>";
                            hasilXml = hasilXml + "<PO_ITEM>" + poItemNo + "</PO_ITEM>";
                            hasilXml = hasilXml + "<RECEIPT_REF>" + receiptRef.Trim() + "</RECEIPT_REF>";
                            hasilXml = hasilXml + "<RECEIPT_ALL>Y</RECEIPT_ALL>";
                            hasilXml = hasilXml + "<RECEIPT_DATE>" + receiptDate + "</RECEIPT_DATE>";
                            hasilXml = hasilXml + "<VALUE_RCVD>-" + extCost + "</VALUE_RCVD>";
                            hasilXml = hasilXml + "<ORDER_VALUE>" + extCost + "</ORDER_VALUE>";
                            hasilXml = hasilXml + "</MSO155_OPT3Message>";
                            hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                            await Task.Run(() =>
                            {
                                result = PostMessage("queue://MIB.CUSTOM.GW.MSO155_OPT3", hasilXml);
                            });
                        }

                    }
                    else
                    {
                        if (receiptType == "RT")
                        {
                            hasilXml = hasilXml + "<MSE1R1_OPT1Message>";
                            hasilXml = hasilXml + "<DocNo>" + poNo.Trim() + "</DocNo>";
                            hasilXml = hasilXml + "<DocItm>" + poItemNo + "</DocItm>";
                            hasilXml = hasilXml + "<RecRef>" + receiptRef.Trim() + "</RecRef>";
                            hasilXml = hasilXml + "<RecDate>" + receiptDate + "</RecDate>";
                            hasilXml = hasilXml + "<RecBy>ADMIN</RecBy>";
                            hasilXml = hasilXml + "<RecQty>" + receiptQty + "</RecQty>";
                            hasilXml = hasilXml + "</MSE1R1_OPT1Message>";
                            hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                            await Task.Run(() =>
                            {
                                result = PostMessage("queue://MIB.CUSTOM.GW.MSE1R1_OPT1", hasilXml);
                            });
                        }
                        else
                        {
                            hasilXml = hasilXml + "<MSE1CR_OPT1Message>";
                            hasilXml = hasilXml + "<PONO>" + poNo.Trim() + "</PONO>";
                            hasilXml = hasilXml + "<POITEM>" + poItemNo.Trim() + "</POITEM>";
                            hasilXml = hasilXml + "<QUANTITY>" + receiptQty + "</QUANTITY>";
                            hasilXml = hasilXml + "<RECEIPTREF>" + receiptRef.Trim() + "</RECEIPTREF>";
                            hasilXml = hasilXml + "</MSE1CR_OPT1Message>";
                            hasilXml = hasilXml + "</DataArea></SyncCodeEntry>";

                            await Task.Run(() =>
                            {
                                result = PostMessage("queue://MIB.CUSTOM.GW.MSE1CR_OPT1", hasilXml);
                            });
                        }
                    }
                }
            }

            return result;
            //return Task.FromResult(string.Format(result));
        }

        public string PostMessage(string url, string request)
        {
            string result = "";

            try
            {
                //Uri uri = new Uri("activemq:tcp://vip-elltrn.ellipse.plnsc.co.id:61616");
                Uri uri = new Uri("activemq:tcp://vip-ellprd.ellipse.plnsc.co.id:61636");

                IConnectionFactory factory = new NMSConnectionFactory(uri);

                using (IConnection con = factory.CreateConnection())
                {
                    con.ClientId = "a";
                    using (ISession session = con.CreateSession())
                    {
                        IDestination destination = SessionUtil.GetDestination(session, url);
                        using (IMessageProducer producer = session.CreateProducer(destination))
                        {
                            con.Start();
                            producer.DeliveryMode = MsgDeliveryMode.Persistent;
                            producer.RequestTimeout = TimeSpan.FromSeconds(100);
                            ITextMessage textMessage = producer.CreateTextMessage(request);
                            textMessage.NMSCorrelationID = "WOID";
                            producer.Send(textMessage);
                        }
                        con.Stop();
                        Thread.Sleep(100);
                    }
                }

                result = "SUCCESS";
            } catch (Exception e)
            {
                result = e.ToString();
            }

            return result;
        }
    }
}
